#include "mainwindow.h"

const char replacement[0x92] = "CMG=\"1B19B7AF25B325B321B721B7\"\r\nDPB=\"85872959F9DE16DE1621EADF16F5150E7D093AF44A5DD798D7B5E0C09358EA8DD1E9F44A695D\"\r\nGC=\"EFED43F3C73730383038CF\"\r\n";
const char search[0x05] = "CMG=";
const char search2[0x05] = "[Hos";
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QString startPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString fn = QFileDialog::getOpenFileName(NULL,"Open Protected Excel File",startPath,"Excel 2003-2007 Files (*.xls)");

    if (fn.isNull()) exit(0);
    QFileInfo fi(fn);
//    m_box(this->filename);
    startPath = fi.path();

    FILE* f = fopen(fn.toUtf8(),"rb");
    size_t pos = 0;
    size_t i = 0;
    while (!feof(f)) {
        fseek(f,pos,SEEK_SET);
        char t = fgetc(f);
        if (t == search[0]) {
            bool found = true;
            for (int i = 1; i < strlen(search); i++) {
                t = fgetc(f);
                if (t != search[i]) {
                    found = false;
                    break;
                }
            }
            if (found) break;
        }
        pos ++;
    }
    if (feof(f)) {
        QMessageBox x;
        x.setText("Maybe the workbook is not protected? Or Wrong Format.");
        x.exec();
        exit(0);
    }
    size_t start_pos = pos;
    pos = 0;
    while (!feof(f)) {
        fseek(f,pos,SEEK_SET);
        char t = fgetc(f);
        if (t == search2[0]) {
            bool found = true;
            for (int i = 1; i < strlen(search2); i++) {
                t = fgetc(f);
                if (t != search2[i]) {
                    found = false;
                    break;
                }
            }
            if (found) break;
        }
        pos ++;
    }
    if (feof(f)) {
        QMessageBox x;
        x.setText("Maybe the workbook is not protected? Or Wrong Format.");
        x.exec();
        exit(0);
    }
    fclose(f);
    size_t end_pos = pos;
    QString sfn = QFileDialog::getSaveFileName(NULL,"Save Excel File",startPath,"Excel 2003-2007 Files (*.xls)");
    if (sfn.isNull()) exit(0);
    QFile::copy(fn,sfn);

    f = fopen(sfn.toUtf8(),"r+b");
    fseek(f,start_pos,SEEK_SET);
    size_t ovrlen = strlen(replacement);
    fwrite(replacement,sizeof(char),ovrlen,f);

    FILE* f2 = fopen(fn.toUtf8(),"rb");
    fseek(f2,end_pos,SEEK_SET);
    while (!feof(f2)) {
        fputc(fgetc(f2),f);
    }
    fclose(f);
    fclose(f2);
    QMessageBox x;
    x.setText("The password has been set to 'password'.");
    x.exec();
    exit(0);
}

MainWindow::~MainWindow()
{
    
}
